Condor Profile for snakemake projects. See e.g. https://gitlab.cern.ch/lvomberg/ttbar_wtaunujets_tauid_sf_run3
This profile works as a set of instructions to snakemake, that teaches it to talk to HTCondor, and set a few command line options that should always be used, but don't need to be typed every time.
Depending on Location where you want to run this, use a different branch. Master is for LXplus, BAF for Bonn Analysis Facility.

### Setup (First time only)
You can use the environment that I have tar-balled at `/eos/user/l/lvomberg/venvs/snakemake.tar.gz`, however I do not guarantee that I won't mess with it. You can untar to a location of your liking and use `conda activate`. If you want to make your own environment, do:
```
cd /your/environment/will/live/here
conda env create -f /path/to/this/repo/snakemake-requirements.yml -p snakemake
tar -czvf snakemake.tar.gz snakemake
```
In `condor_submit.py` you will have to adjust from where the jobs pull the tarball. Adjust the line accordingly:
```
transfer_input_files = root://eosuser.cern.ch//eos/user/l/lvomberg/venvs/snakemake.tar.gz 
```
### Usage
To use this profile, call snakemake with `--profile /path/to/this/repo`. If you do not want to do that, you can set an environment variable:
```
export SNAKEMAKE_PROFILE="path/to/this/repo"
```
As long as you do not specify something else explicitely on the command line, you will always use this profile. 
Adjust the config to your needs, best to check documentation of snakemake for this. 