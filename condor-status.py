#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2019 Matthew Stone <mrstone3@wisc.edu>
# Distributed under terms of the MIT license.

"""

"""
import argparse
import subprocess
import re
from datetime import datetime


def main():
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("jobid")
    args = parser.parse_args()

    # if jobs are still running, need condor_q
    condor_q = ["condor_q", args.jobid, "-autoformat:r", "JobStatus"]

    try:
        res_q = subprocess.run(condor_q, check=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError:
        # sometimes scheduler is busy and does not return proper job status
        # simply assume it is still running and try again in the next call to this
        print("running")
        exit()

    # parse output:
    jobStatus = re.findall(r"\d+", str(res_q.stdout))
    if jobStatus:
        # check condor_q
        if isinstance(jobStatus, list):
            jobStatus = jobStatus[0]

        jobStatus = int(jobStatus)
        # see https://pages.cs.wisc.edu/~adesmet/status.html#condor-jobstatus
        if jobStatus in [
            0,
            1,
            2,
            6,
        ]:  # unexpanded, idle, running, removing, transferrring output
            print("running")
        elif jobStatus in [4]:  # completed
            print("success")
        elif jobStatus in [3, 5, 7]:  # removed, held, suspended
            print("failed")
        else:
            print(
                f"status command:\n {condor_q}\n resulted in unknown jobStatus: {jobStatus}"
            )

    else:
        # check if job is already finished and in history instead
        condor_history = [
            "condor_history",
            args.jobid,
            "-autoformat:r",
            "JobStatus",
            "-limit",
            "1",
        ]
        try:
            res_h = subprocess.run(condor_history, check=True, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError:
            # sometimes scheduler is busy and does not return proper job status
            # simply assume it is still running and try again in the next call to this
            print("running")
            exit()
        # parse output:
        jobStatus = re.findall(r"\d+", str(res_h.stdout))
        if not jobStatus:
            print("Job not found in active jobs or history")
            print("failed")
        if isinstance(jobStatus, list):
            jobStatus = jobStatus[0]
        jobStatus = int(jobStatus)
        if jobStatus == 4:
            print("success")
        elif jobStatus in [3, 5, 6, 7]:
            print("failed")
        else:
            print(
                f"history command:\n {condor_history}\n resulted in unknown jobStatus: {jobStatus}"
            )


if __name__ == "__main__":
    main()
